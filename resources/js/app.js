import './bootstrap';
import 'flowbite';
import Datepicker from 'flowbite-datepicker/Datepicker';
import 'openiban';
import * as te from 'tw-elements';
import './iban';
import Alpine from 'alpinejs';


window.Alpine = Alpine;
window.Datepicker = Datepicker;
window.te = te;

Alpine.start();
