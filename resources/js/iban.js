const iban = document.getElementById('iban').addEventListener('change', function () {
    const iban = document.getElementById('iban').value;
    const bic = document.getElementById('bic');
    const bank = document.getElementById('bank');
    Openiban.validate(iban).then((result) => {
        console.log(result);
        if (result.valid) {
            bic.value = result.bankData.bic;
            bank.value = result.bankData.name;
        } else {
            bic.value = '';
            bank.value = '';
        }
    });
});
