<x-club-layout>
    <div class="md:container md:mx-auto">
        <div class="content-center md:w-4/6 mx-auto my-3">
            <div class="isolate bg-white px-6 py-24 sm:py-32 lg:px-8">
                <div
                    class="absolute inset-x-0 top-[-10rem] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[-20rem]"
                    aria-hidden="true">
                    <div
                        class="relative left-1/2 -z-10 aspect-[1155/678] w-[36.125rem] max-w-none -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-40rem)] sm:w-[72.1875rem]"
                        style="clip-path: polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)"></div>
                </div>
                <div class="mx-auto max-w-2xl text-center">
                    <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Förderverein Michaelschule
                        Tinnen e.V.</h2>
                    <p class="mt-2 text-lg leading-8 text-gray-600">Online Beitrittserklärung</p>
                </div>
                @if (session('success'))
                    <div class="bg-green-400 text-white">
                        {{ session('success') }}
                    </div>
                @endif
                <form action="{{ route('foerderverein.store') }}" method="POST" class="mx-auto mt-16 max-w-xl sm:mt-20"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                        <div>
                            <label for="first-name"
                                   class="block text-sm font-semibold leading-6 text-gray-900">Vorname</label>
                            <div class="mt-2.5">
                                <input type="text" name="first-name" id="first-name" autocomplete="given-name"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                            @error('first-name')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="last-name"
                                   class="block text-sm font-semibold leading-6 text-gray-900">Nachname</label>
                            <div class="mt-2.5">
                                <input type="text" name="last-name" id="last-name" autocomplete="family-name"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                            @error('last-name')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="sm:col-span-2">
                            <label for="street"
                                   class="block text-sm font-semibold leading-6 text-gray-900">Straße</label>
                            <div class="mt-2.5">
                                <input type="text" name="street" id="street" autocomplete="organization"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                            @error('street')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="street"
                                   class="block text-sm font-semibold leading-6 text-gray-900">Wohnort</label>
                            <div class="mt-2.5">
                                <input type="text" name="location" id="location" autocomplete="organization"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                            @error('location')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="plz" class="block text-sm font-semibold leading-6 text-gray-900">PLZ</label>
                            <div class="mt-2.5">
                                <input type="text" name="plz" id="plz" autocomplete="organization"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                            @error('plz')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="sm:col-span-2">
                            <label for="email" class="block text-sm font-semibold leading-6 text-gray-900">Email</label>
                            <div class="mt-2.5">
                                <input type="email" name="email" id="email" autocomplete="email"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                            @error('email')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <hr class="sm:col-span-2">
                        <h2>Bankdaten</h2>
                        <div class="sm:col-span-2">
                            <label for="bank" class="block text-sm font-semibold leading-6 text-gray-900">Kontoinhaber
                                (falls abweichend)</label>
                            <div class="mt-2.5">
                                <input type="text" name="account" id="account" autocomplete="organization"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                        </div>
                        <div class="sm:col-span-2">
                            <label for="iban" class="block text-sm font-semibold leading-6 text-gray-900">IBAN</label>
                            <div class="mt-2.5">
                                <input type="text" name="iban" value="" id="iban" autocomplete="organization"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                                @error('iban')
                                <div class="text-red-700">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="sm:col-span-2">
                            <label for="bank" class="block text-sm font-semibold leading-6 text-gray-900">Bank</label>
                            <div class="mt-2.5">
                                <input type="text" name="bank" id="bank" autocomplete="organization" readonly="readonly"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                        </div>
                        <div class="sm:col-span-2">
                            <label for="bic" class="block text-sm font-semibold leading-6 text-gray-900">BIC</label>
                            <div class="mt-2.5">
                                <input type="text" name="bic" id="bic" autocomplete="organization" readonly="readonly"
                                       class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            </div>
                        </div>
                        <div>
                            <label for="price" class="block text-sm font-medium leading-6 text-gray-900">Mitgliedsbeitrag</label>
                            <div class="relative mt-2 rounded-md shadow-sm">
                                <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                    <span class="text-gray-500 sm:text-sm">€</span>
                                </div>
                                <input type="text" name="price" id="price"
                                       class="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                       placeholder="18.00">
                            </div>
                            <sub>Mindestbeitrag 18€</sub>
                            @error('price')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="sepa" class="block text-sm font-semibold leading-6 text-gray-900">Einverständnis
                                SEPA</label>
                            <div class="mt-2.5">
                                <input type="checkbox" name="sepa" id="sepa" autocomplete="organization"
                                       class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500">
                            </div>
                            <sub>Ich ermächtige den Förderverein Michaelschule Tinnen e.V., die von mir zu entrichtenden
                                Zahlungen bei Fälligkeit von meinem Konto mittels Lastschrift einzuziehen.</sub>
                            @error('sepa')
                            <div class="text-red-700">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="pt-3 pb-3">
                        <label for="statute" class="block text-sm font-semibold leading-6 text-gray-900">Einverständnis
                            Satzung</label>
                        <div class="mt-2.5">
                            <input type="checkbox" name="statute" id="statute" autocomplete="organization"
                                   class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500">
                        </div>
                        <sub>Hiermit bin ich mit der Satzung des Förderverein Förderverein Michaelschule Tinnen e.V.
                            einverstanden.</sub>
                        @error('statute')
                        <div class="text-red-700">{{ $message }}</div>
                        @enderror
                    </div>
                    <hr class="sm:col-span-2">
                    <div class="sm:col-span-2">
                        <label for="message"
                               class="block text-sm font-semibold leading-6 text-gray-900">Kommentar</label>
                        <div class="mt-2.5">
                                <textarea name="message" id="message" rows="4"
                                          class="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"></textarea>
                        </div>
                    </div>
                    <div class="mt-10">
                        <button type="submit"
                                class="block w-full rounded-md bg-indigo-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                            Senden
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-club-layout>
