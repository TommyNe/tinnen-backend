<div>
**Name:
{{ $request->input('last-name') }}
<br>
**Vorname:
{{ $request->input('first-name') }}
<br>
**Straße:
{{ $request->input('street') }}
<br>
**Wohnort:
{{ $request->input('location') }} {{ $request->input('plz') }}
<br>
**Bank:
{{ $request->input('bank') }}
<br>
**BIC:
{{ $request->input('bic') }}
<br>
**IBAN:
{{ $request->input('iban') }}
<br>
**Mindestbeitrag:
{{ $request->input('price') }}
<br>
**Einverständnis SEPA:
{{ $request->input('sepa') }}
<br>
**Einverständnis Satzung:
{{ $request->input('statute') }}
<br>
**Email:
{{ $request->input('email') }}
<br>
**Kommentar:
{{ $request->input('message') }}
<br>
</div>
