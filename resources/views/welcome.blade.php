<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apple-itunes-app" content="app-id=6444919954">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <link rel="icon" type="image/x-icon" href="favicon.ico">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

</head>
<body class="antialiased">
<div
    class="relative flex items-top justify-center min-h-screen bg-[#e8f2d7] dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div>
            <div>
                <img src="logo.png">
            </div>
            <div class="flex justify-center gap-6">
                <a href="https://play.google.com/store/apps/details?id=com.tommy1984.tinnenapp&hl=de_CH"><img src="storage/google-play-badge.png"
                                                                                             alt="google-play"
                                                                                             width="120"
                                                                                             height="40"/></a>
                <a href="https://apps.apple.com/tr/app/tinnen/id6444919954"><img
                        src="storage/Download_on_the_App_Store_Badge_DE_RGB_blk_092917.svg" alt="iOS"/></a>
            </div>
        </div>
    </div>

</div>
</body>
</html>
