<x-app-layout>
    <div class="content-center m-3">
        <div class="shadow sm:overflow-hidden sm:rounded-md">
            <div class="space-y-6 bg-white px-4 py-5 sm:p-6 container mx-auto">
                <div class="text-center">
                    <h1 class="text-2xl">Impressum</h1>
                </div>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @foreach($impressum as $item)
                    <div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-lg flex items-center space-x-4">
                        <div>
                            <div class="text-xl font-bold text-black">{{ $item->title }}</div>
                            <hr/>
                            <p class="text-slate-500 text-xs pt-2">{{ $item->created_at->format('d.m.Y') }} <span class="bg-slate-500 rounded text-neutral-50 px-1">{{ $item->verein }}</span></p>
                            <p class="pt-1">{{ Str::limit($item->content, 40) }}</p>
                            <hr/>
                            <form action="{{ route('news.destroy',$item->id) }}" method="Post">
                                <a class="text-sky-500" href="{{ route('news.edit',$item->id) }}">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="text-red-500">Delete</button>
                            </form>
                        </div>
                    </div>
                @endforeach

                <div class="flex justify-center">
                    <a href="{{ route('impressum.create') }}"> <button class="rounded-lg p-2 text-white font-semibold bg-sky-500 hover:bg-sky-600">Neuer Eintrag</button></a>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
