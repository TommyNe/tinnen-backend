<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Impressum</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

</head>
<body class="antialiased">
<div
    class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ url('/dashboard') }}"
                   class="text-sm text-gray-700 dark:text-gray-500 underline">Dashboard</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                {{--@if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif--}}
            @endauth
        </div>
    @endif
    <div>
        <h1 class="text-2xs">Impressum</h1>

        <h2>Angaben gem&auml;&szlig; &sect; 5 TMG</h2>
        <p>Thomas Nehrenberg<br />
            Am Tinner Loh 5<br />
            49733 Haren</p>

        <h2>Kontakt</h2>
        <p>Telefon: 05932 5003384<br />
            E-Mail: thomas.nehrenberg@me.com</p>

        <h2>Redaktionell verantwortlich</h2>
        <p>Thomas Nehrenberg<br />
            Am Tinner Loh 5<br />
            49733 Haren</p>

        <p>Quelle: <a href="https://www.e-recht24.de">eRecht24</a></p>
    </div>

</div>
</body>
</html>
