<x-app-layout>
    <div class="content-center md:w-4/6 mx-auto my-3">
        <form action="{{route('impressum.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow sm:overflow-hidden sm:rounded-md">
                <div class="space-y-6 bg-white px-4 py-5 sm:p-6">
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="title" class="block text-sm font-medium text-gray-700">Titel</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="title" id="title"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="Titel">
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="content" class="block text-sm font-medium text-gray-700">Inhalt</label>
                        <div class="mt-1">
                            <textarea id="content" name="content" rows="3"
                                      class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                      placeholder="Inhalt..."></textarea>
                        </div>
                        <p class="mt-2 text-sm text-gray-500">Textinhalt/Bericht</p>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 text-right sm:px-6">
                    <button type="submit"
                            class="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                        Speichern
                    </button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>
