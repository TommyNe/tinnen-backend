<x-app-layout>
    <div class="content-center m-3">
        <div class="shadow sm:overflow-hidden sm:rounded-md">
            <div class="space-y-6 bg-white px-4 py-5 sm:p-6 container mx-auto">
                <div class="text-center">
                    <h1 class="text-2xl font-bold underline">Aktuell</h1>
                </div>

                @if ($message = Session::get('success'))
                    <div class="flex justify-center space-x-2">
                        <div
                            class="pointer-events-auto mx-auto mb-4 hidden w-96 max-w-full rounded-lg bg-success-100 bg-clip-padding text-sm text-success-700 shadow-lg shadow-black/5 data-[te-toast-show]:block data-[te-toast-hide]:hidden"
                            id="static-example"
                            role="alert"
                            aria-live="assertive"
                            aria-atomic="true"
                            data-te-autohide="true"
                            data-te-toast-init
                            data-te-toast-show>
                            <div
                                class="flex items-center justify-between rounded-t-lg border-b-2 border-success/20 bg-success-100 bg-clip-padding px-4 pt-2.5 pb-2">
                                <p class="flex items-center font-bold text-success-700">
                                    Aktuelles
                                </p>
                                <div class="flex items-center">
                                    <button
                                        type="button"
                                        class="ml-2 box-content rounded-none border-none opacity-80 hover:no-underline hover:opacity-75 focus:opacity-100 focus:shadow-none focus:outline-none"
                                        data-te-toast-dismiss
                                        aria-label="Close">
                                      <span
                                          class="w-[1em] focus:opacity-100 disabled:pointer-events-none disabled:select-none disabled:opacity-25 [&.disabled]:pointer-events-none [&.disabled]:select-none [&.disabled]:opacity-25">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke-width="1.5"
                                            stroke="currentColor"
                                            class="h-6 w-6">
                                          <path
                                              stroke-linecap="round"
                                              stroke-linejoin="round"
                                              d="M6 18L18 6M6 6l12 12"/>
                                        </svg>
                                      </span>
                                    </button>
                                </div>
                            </div>
                            <div
                                class="break-words rounded-b-lg bg-success-100 py-4 px-4 text-success-700">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="flex justify-center">
                    <a href="{{ route('news.create') }}">  <button
                            class="ml-1 flex justify-start items-center inline-block rounded bg-sky-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                 stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15"/>
                            </svg>
                            <span class="text-base ml-3">Push Nachricht</span>
                        </button>
                    </a>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
