<x-app-layout>
    <div class="content-center md:w-4/6 mx-auto my-3">
        <form action="{{route('news.update', $news->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="shadow sm:overflow-hidden sm:rounded-md">
                <div class="space-y-6 bg-white px-4 py-5 sm:p-6">
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="title" class="block text-sm font-medium text-gray-700">Titel</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="title" id="title"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       value="{{$news->title}}">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="verein" class="block text-sm font-medium text-gray-700">Aktuelles von</label>
                            <div class="mb-3 xl:w-96">
                                <select name="verein" id="verein" class="form-select appearance-none
      block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" aria-label="Default select example">
                                    @if($news->verein)
                                        <option value="{{$news->verein}}" selected>{{ $news->verein }}</option>
                                    @else
                                        <option selected>Bitte angeben</option>
                                    @endif
                                    <option value="Allgemein">Allgemein</option>
                                    <option value="DJK Tinnen">DJK Tinnen</option>
                                    <option value="St. Bernadus">St. Bernadus</option>
                                    <option value="ETWAH">ETWAH</option>
                                    <option value="Tinner Jäger">Tinner Jäger</option>
                                    <option value="Jugend Blasorchester">Jugend Blasorchester</option>
                                    <option value="Oltimer Club">Oldtimer Club</option>
                                    <option value="Motorrad Club">Motorrad Club</option>
                                    <option value="Imker Verein">Imker Verein</option>
                                    <option value="KLJB Tinnen">KLJB Tinnen</option>
                                    <option value="Michael Schule">Michael Schule</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="description" class="block text-sm font-medium text-gray-700">Inhalt</label>
                        <div class="mt-1">
                            <textarea id="message" name="description" rows="3"
                                      class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                      placeholder="Inhalt...">{{ $news->message }}</textarea>
                        </div>
                        <p class="mt-2 text-sm text-gray-500">Textinhalt/Bericht</p>
                    </div>
                    <div>
                        <label class="block text-sm font-medium text-gray-700">Titelbild</label>
                        <div
                            class="mt-1 flex justify-center rounded-md border-2 border-dashed border-gray-300 px-6 pt-5 pb-6">
                            <div class="space-y-1 text-center">
                                <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none"
                                     viewBox="0 0 48 48" aria-hidden="true">
                                    <path
                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <img id="output"/>
                                <div class="flex text-sm text-gray-600">
                                    <label for="file-upload"
                                           class="relative cursor-pointer rounded-md bg-white font-medium text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-500 focus-within:ring-offset-2 hover:text-indigo-500">
                                        <span>Lade ein Bild hoch</span>
                                        <input id="file-upload" name="file" type="file" class="sr-only" onchange="loadFile(event)">
                                    </label>
                                </div>
                                <p class="text-xs text-gray-500">PNG, JPG, GIF bis maximal 10MB</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 text-right sm:px-6">
                    <button type="submit"
                            class="rounded-lg p-2 text-white font-semibold bg-sky-500 hover:bg-sky-600">
                        Speichern
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
</x-app-layout>
