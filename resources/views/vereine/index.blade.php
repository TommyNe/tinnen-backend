<x-app-layout>
    <div class="content-center m-3">
        <div class="shadow sm:overflow-hidden sm:rounded-md">
            <div class="space-y-6 bg-white px-4 py-5 sm:p-6 container mx-auto">
                <div class="text-center">
                    <h1 class="text-2xl font-bold underline">Vereine</h1>
                </div>
                @if ($message = Session::get('success'))
                    <div class="flex justify-center space-x-2">
                        <div
                            class="pointer-events-auto mx-auto mb-4 hidden w-96 max-w-full rounded-lg bg-success-100 bg-clip-padding text-sm text-success-700 shadow-lg shadow-black/5 data-[te-toast-show]:block data-[te-toast-hide]:hidden"
                            id="static-example"
                            role="alert"
                            aria-live="assertive"
                            aria-atomic="true"
                            data-te-autohide="true"
                            data-te-toast-init
                            data-te-toast-show>
                            <div
                                class="flex items-center justify-between rounded-t-lg border-b-2 border-success/20 bg-success-100 bg-clip-padding px-4 pt-2.5 pb-2">
                                <p class="flex items-center font-bold text-success-700">
                                    Vereine
                                </p>
                                <div class="flex items-center">
                                    <button
                                        type="button"
                                        class="ml-2 box-content rounded-none border-none opacity-80 hover:no-underline hover:opacity-75 focus:opacity-100 focus:shadow-none focus:outline-none"
                                        data-te-toast-dismiss
                                        aria-label="Close">
          <span
              class="w-[1em] focus:opacity-100 disabled:pointer-events-none disabled:select-none disabled:opacity-25 [&.disabled]:pointer-events-none [&.disabled]:select-none [&.disabled]:opacity-25">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="h-6 w-6">
              <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M6 18L18 6M6 6l12 12"/>
            </svg>
          </span>
                                    </button>
                                </div>
                            </div>
                            <div
                                class="break-words rounded-b-lg bg-success-100 py-4 px-4 text-success-700">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                @endif

                <div class="flex justify-center">
                    <a href="{{ route('vereine.create') }}">
                        <button
                            class="ml-1 flex justify-start items-center inline-block rounded bg-sky-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                 stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15"/>
                            </svg>
                            <span class="text-base ml-3">Neuer Eintrag</span>
                        </button>
                    </a>

                </div>
                <tbody>
                @foreach($vereine as $item)
                    <div class="p-6 max-w-m mx-auto bg-white rounded-xl shadow-lg flex items-center space-x-4">
                        <div class="shrink-0">
                            <img class="h-12 w-12" src="logos/{{ $item->logo }}" alt="Vereins Logo">
                        </div>
                        <div>
                            <div class="text-xl font-bold text-black">{{ $item->verein}}</div>
                            <hr/>
                            <p class="text-slate-500 text-xs pt-2">Geändert
                                am {{ $item->updated_at->format('d.m.Y') }}</p>
                            <p class="pt-1">{{ Str::limit($item->beschreibung, 40) }}</p>
                            <hr/>
                            <a class="text-sky-500" href="{{ route('vereine.edit',$item->id) }}">Edit</a>
                            <button class="text-red-500" type="button" data-modal-toggle="popup-modal{{$item->id}}">
                                Delete
                            </button>
                        </div>
                    </div>


                    <div id="popup-modal{{$item->id}}" tabindex="-1"
                         class="fixed top-0 left-0 right-0 z-50 hidden p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
                        <div class="relative w-full h-full max-w-md md:h-auto">
                            <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                <button type="button"
                                        class="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                                        data-modal-toggle="popup-modal{{$item->id}}">
                                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                              clip-rule="evenodd"></path>
                                    </svg>
                                    <span class="sr-only">Close modal</span>
                                </button>
                                <div class="p-6 text-center">
                                    <svg aria-hidden="true"
                                         class="mx-auto mb-4 text-gray-400 w-14 h-14 dark:text-gray-200" fill="none"
                                         stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                    </svg>
                                    <h3 class="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">Wollen Sie den Verein wirklich löschen?</h3>
                                    <form action="{{ route('vereine.destroy',$item->id) }}" method="Post">
                                        @csrf
                                        @method('DELETE')
                                        <button data-modal-toggle="popup-modal{{$item->id}}" type="submit"
                                                class="text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2">
                                            Löschen
                                        </button>
                                        <button data-modal-toggle="popup-modal{{$item->id}}" type="button"
                                                class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                                           Abbrechen
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                @endforeach
            </div>
        </div>
    </div>
</x-app-layout>
