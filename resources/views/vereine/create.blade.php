<x-app-layout>
    <div class="content-center md:w-4/6 mx-auto my-3">
        <form action="{{route('vereine.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow sm:overflow-hidden sm:rounded-md">
                <div class="space-y-6 bg-white px-4 py-5 sm:p-6">
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="vereine" class="block text-sm font-medium text-gray-700">Verein</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="vereine" id="title"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="Verein...">
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="description" class="block text-sm font-medium text-gray-700">Beschreibung</label>
                        <div class="mt-1">
                            <textarea id="description" name="description" rows="3"
                                      class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                      placeholder="Beschreibung..."></textarea>
                        </div>
                        <p class="mt-2 text-sm text-gray-500">Über den Verein</p>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="url" class="block text-sm font-medium text-gray-700">Website</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="url" id="url"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="https://">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="facebook" class="block text-sm font-medium text-gray-700">Facebook</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="facebook" id="facebook"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="https://facebook.com/vereinsname">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="instagram" class="block text-sm font-medium text-gray-700">Instagram</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="instagram" id="instagram"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="https://instagram.com/vereinsname">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="email" class="block text-sm font-medium text-gray-700">E-Mail</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="email" id="email"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="max@mustermann.de">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="tel" class="block text-sm font-medium text-gray-700">Tel.</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="tel" id="tel"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="05932/xxxx">
                            </div>
                        </div>
                    </div>
                    <div>
                        <label class="block text-sm font-medium text-gray-700">Vereinslogo</label>
                        <div
                            class="mt-1 flex justify-center rounded-md border-2 border-dashed border-gray-300 px-6 pt-5 pb-6">
                            <div class="space-y-1 text-center">
                                <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none"
                                     viewBox="0 0 48 48" aria-hidden="true">
                                    <path
                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <div class="flex text-sm text-gray-600">
                                    <label for="file"
                                           class="relative cursor-pointer rounded-md bg-white font-medium text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-500 focus-within:ring-offset-2 hover:text-indigo-500">
                                        <span>Upload a file</span>
                                        <input id="file" name="file" type="file" class="sr-only">
                                    </label>
                                    <p class="pl-1">or drag and drop</p>
                                </div>
                                <p class="text-xs text-gray-500">PNG, JPG, GIF up to 10MB</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 text-right sm:px-6">
                    <button type="submit"
                            class="rounded-lg p-2 text-white font-semibold bg-sky-500 hover:bg-sky-600">
                        Speichern
                    </button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>
