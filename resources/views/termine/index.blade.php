<x-app-layout>
    <div class="content-center m-3">
        <div class="shadow sm:overflow-hidden sm:rounded-md">
            <div class="space-y-6 bg-white px-4 py-5 sm:p-6 container mx-auto">
                <div class="text-center">
                    <h1 class="text-2xl font-bold underline">Termine</h1>
                </div>
                @if ($message = Session::get('success'))
                    <div class="flex justify-center space-x-2">
                        <div
                            class="pointer-events-auto mx-auto mb-4 hidden w-96 max-w-full rounded-lg bg-success-100 bg-clip-padding text-sm text-success-700 shadow-lg shadow-black/5 data-[te-toast-show]:block data-[te-toast-hide]:hidden"
                            id="static-example"
                            role="alert"
                            aria-live="assertive"
                            aria-atomic="true"
                            data-te-autohide="true"
                            data-te-toast-init
                            data-te-toast-show>
                            <div
                                class="flex items-center justify-between rounded-t-lg border-b-2 border-success/20 bg-success-100 bg-clip-padding px-4 pt-2.5 pb-2">
                                <p class="flex items-center font-bold text-success-700">
                                    Termine
                                </p>
                                <div class="flex items-center">
                                    <button
                                        type="button"
                                        class="ml-2 box-content rounded-none border-none opacity-80 hover:no-underline hover:opacity-75 focus:opacity-100 focus:shadow-none focus:outline-none"
                                        data-te-toast-dismiss
                                        aria-label="Close">
                                        <span class="w-[1em] focus:opacity-100 disabled:pointer-events-none disabled:select-none disabled:opacity-25 [&.disabled]:pointer-events-none [&.disabled]:select-none [&.disabled]:opacity-25">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                stroke-width="1.5"
                                                stroke="currentColor"
                                                class="h-6 w-6">
                                              <path
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round"
                                                  d="M6 18L18 6M6 6l12 12"/>
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div
                                class="break-words rounded-b-lg bg-success-100 py-4 px-4 text-success-700">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                @endif


                <div class="flex justify-center">
                    <a href="{{ route('termine.create') }}">
                        <button
                            class="ml-1 flex justify-start items-center inline-block rounded bg-sky-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                 stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15"/>
                            </svg>
                            <span class="text-base ml-3">Neuer Eintrag</span>
                        </button>
                    </a>
                </div>

                @foreach($termine as $item)
                    <div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-lg flex items-center space-x-4">
                        <div>
                            <div class="text-xl font-bold text-black">{{ $item->name }}</div>
                            <p class="text-slate-500 text-medium">
                                Am {{ date('d M Y H:i', strtotime($item->datum)) }}</p>
                            <p><span class="bg-slate-500 rounded text-neutral-50 px-1">{{ $item->verein }}</span></p>
                            <hr/>
                            <p>{{ Str::limit($item->beschreibung, 40) }}</p>
                            <hr/>
                            <form action="{{ route('termine.destroy',$item->id) }}" method="Post">
                                <div class="flex justify-start">
                                    <div>
                                        <a class="text-sky-500 " href="{{ route('termine.edit',$item->id) }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                 stroke-width="{1.5}" stroke="currentColor" class="w-6 h-6">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"/>
                                            </svg>
                                        </a>
                                    </div>
                                    @csrf
                                    @method('DELETE')
                                    <div>
                                        <button type="submit" class="text-red-500">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                 stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
                {{ $termine->links() }}
            </div>
        </div>
    </div>
</x-app-layout>
