<x-app-layout>
    <div class="content-center md:w-4/6 mx-auto my-3">
        <form action="{{route('termine.store')}}" method="POST">
            @csrf
            <div class="shadow sm:overflow-hidden sm:rounded-md">
                <div class="space-y-6 bg-white px-4 py-5 sm:p-6">
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="title" class="block text-sm font-medium text-gray-700">Event</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="title" id="title"
                                       class="block w-full flex-1 rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="Veranschtaltung...">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="verein" class="block text-sm font-medium text-gray-700">Verein</label>
                            <div class="mb-3 xl:w-96">
                                <select name="verein" id="verein" class="form-select appearance-none
      block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" aria-label="Default select example">
                                    <option selected>Verein</option>
                                    <option value="Allgemein">Allgemein</option>
                                    <option value="DJK Tinnen">DJK Tinnen</option>
                                    <option value="St. Bernadus">St. Bernadus</option>
                                    <option value="ETWAH">Kirchengemeinde</option>
                                    <option value="Tinner Jäger">Tinner Jäger</option>
                                    <option value="Jugend Blasorchester">Jugend Blasorchester</option>
                                    <option value="Oltimer Club">Oldtimer Club</option>
                                    <option value="Motorrad Club">Motorrad Club</option>
                                    <option value="Imker Verein">Imker Verein</option>
                                    <option value="KLJB Tinnen">KLJB Tinnen</option>
                                    <option value="Michael Schule">Michael Schule</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="description" class="block text-sm font-medium text-gray-700">Beschreibung</label>
                        <div class="mt-1">
                            <textarea id="description" name="description" rows="3"
                                      class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                      placeholder="Beschreibung..."></textarea>
                        </div>
                        <p class="mt-2 text-sm text-gray-500">Beschreibung des Events</p>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="company-website" class="block text-sm font-medium text-gray-700">Wann</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="datetime-local" name="date"
                                       class="block w-full flex-1 rounded-none rounded-r-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       value="{{ $datetime->format('Y-m-d\Th:i') }}">
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-6">
                        <div class="col-span-3 sm:col-span-2">
                            <label for="company-website" class="block text-sm font-medium text-gray-700">Wo</label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <input type="text" name="eventOrt"
                                       class="block w-full flex-1 rounded-none rounded-r-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                       placeholder="Ort">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 text-right sm:px-6">
                    <button type="submit"
                            class="rounded-lg p-2 text-white font-semibold bg-sky-500 hover:bg-sky-600">
                        Speichern
                    </button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>
