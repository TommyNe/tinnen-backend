<?php

use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\API\DatenschutzController;
use App\Http\Controllers\API\EmergencyController;
use App\Http\Controllers\API\HistorieController;
use App\Http\Controllers\API\ImageController;
use App\Http\Controllers\API\ImpressumController;
use App\Http\Controllers\API\MatchesController;
use App\Http\Controllers\API\NewsController;
use App\Http\Controllers\API\SponsorController;
use App\Http\Controllers\API\TermineController;
use App\Http\Controllers\API\VereineController;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('aktuell', [NewsController::class, 'index']);
Route::get('aktuell/{news}', [NewsController::class, 'show']);

Route::get('termine', [TermineController::class, 'index']);
Route::get('termine/{termine}', [TermineController::class, 'show']);

Route::get('vereine', [VereineController::class, 'index']);
Route::get('vereine/{vereine}', [VereineController::class, 'show']);

Route::get('sponsor', [SponsorController::class, 'index']);

Route::get('image', [ImageController::class, 'index']);

Route::get('emergency', [EmergencyController::class, 'index']);

Route::get('historie', [HistorieController::class, 'index']);

Route::get('impressum', [ImpressumController::class, 'index']);

Route::get('datenschutz', [DatenschutzController::class, 'index']);

Route::get('liveticker', [MatchesController::class, 'index']);
Route::post('/token', [ApiController::class, 'token']);

Route::get('push', [NotificationController::class, 'sendPushNotification']);
Route::middleware('throttle:tokens')->group(function () {});
