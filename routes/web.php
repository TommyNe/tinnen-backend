<?php

use App\Http\Controllers\ClubFormController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\TermineController;
use App\Http\Controllers\VereineController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/impressum', function () {
    return view('impressum.impressum');
});

Route::get('/datenschutz', function () {
    return view('datenschutz.datenschutz');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::resource('news', NewsController::class)
    ->middleware(['auth', 'verified']);

Route::resource('termine', TermineController::class)
    ->middleware('auth', 'verified');

Route::resource('vereine', VereineController::class)
    ->middleware('auth', 'verified');

Route::resource('foerderverein', ClubFormController::class);

require __DIR__.'/auth.php';
