<?php

namespace Database\Factories;

use App\Models\EmergencyAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmergencyAddressFactory extends Factory
{
    protected $model = EmergencyAddress::class;

    public function definition(): array
    {
        return [

        ];
    }
}
