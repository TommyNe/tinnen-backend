<?php

namespace Database\Factories;

use App\Models\Termine;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class TermineFactory extends Factory
{
    protected $model = Termine::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'beschreibung' => $this->faker->word(),
            'verein' => $this->faker->word(),
            'datum' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'eventOrt' => $this->faker->word(),
        ];
    }
}
