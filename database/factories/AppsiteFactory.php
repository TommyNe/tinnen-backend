<?php

namespace Database\Factories;

use App\Models\Appsite;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppsiteFactory extends Factory
{
    protected $model = Appsite::class;

    public function definition(): array
    {
        return [

        ];
    }
}
