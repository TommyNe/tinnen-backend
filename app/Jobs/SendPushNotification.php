<?php

namespace App\Jobs;

use App\Http\Services\Facades\ExpoPushNotification;
use App\Models\Device;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        private readonly string $id,
        private readonly string $title,
        private readonly string $message,
        private readonly string $route,
    ) {
        //
    }

    public function handle(): void
    {
        $deepLink = [
            'deepLink' => [
                'route' => $this->route,
                'params' => [
                    'id' => $this->id,
                ],
            ],
        ];

        foreach (Device::all()->chunk(100) as $group) {
            $device = $group->where('is_active', '=', true)->pluck('token')->toArray();
            ExpoPushNotification::sendPushNotification($device, $this->title, strip_tags($this->message), $deepLink);
        }
    }
}
