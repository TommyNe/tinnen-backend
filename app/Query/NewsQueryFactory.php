<?php

namespace App\Query;

use App\Models\News;

class NewsQueryFactory
{
    public static function getNewsQuery()
    {
        $news = News::query();
        $news->orderBy('created_at', 'desc');

        return $news;
    }
}
