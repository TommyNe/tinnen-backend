<?php

namespace App\Query;

class HistorieQueryFactory
{
    public static function getHistorieQuery()
    {
        $historie = Historie::query()->where('content_id', 'historie');
        $historie->orderBy('created_at', 'desc');

        return $historie;
    }
}
