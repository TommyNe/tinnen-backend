<?php

namespace App\Query;

use App\Models\Image;

class ImageQueryFactory
{
    public static function getImageQuery()
    {
        $image = Image::query();
        $image->orderBy('created_at', 'desc')->limit(4);

        return $image;
    }
}
