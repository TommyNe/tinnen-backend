<?php

namespace App\Query;

use App\Models\Sponsor;

class SponsorQueryFactory
{
    public static function getSponsorQuery()
    {
        $sponsor = Sponsor::query();
        $sponsor->orderBy('created_at', 'desc');

        return $sponsor;
    }
}
