<?php

namespace App\Query;

use App\Models\Matche;

class MatchesQueryFactory
{
    public static function getMatchesQuery()
    {
        $matches = Matche::query();
        $matches->orderBy('created_at', 'asc');

        return $matches;
    }
}
