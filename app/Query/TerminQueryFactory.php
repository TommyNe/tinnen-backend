<?php

namespace App\Query;

use App\Models\Termine;
use Carbon\Carbon;

class TerminQueryFactory
{
    public static function getTerminQuery()
    {
        $now = Carbon::now();
        $termine = Termine::query();
        $termine->orderBy('datum', 'asc')->where('datum', '>=', $now);

        return $termine;
    }
}
