<?php

namespace App\Query;

use App\Models\Emergency;

class EmergencyQueryFactory
{
    public static function getEmergencyQuery()
    {
        $emergency = Emergency::query();
        $emergency->orderBy('created_at', 'desc');

        return $emergency;
    }
}
