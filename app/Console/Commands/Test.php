<?php

namespace App\Console\Commands;

use App\Jobs\SendPushNotification;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $this->info('Test File.');

        SendPushNotification::dispatch(1, 'Wartung beendet!', 'Die App steht wieder im vollen Umfang zu verfügung!', 'event');

        return 0;
    }
}
