<?php

namespace App\Console\Commands;

use App\Jobs\SendPushNotification;
use App\Models\Termine;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $today = Carbon::today();
        $events = Termine::whereDate('datum', $today)->get();

        $this->info('Found '.$events->count().' events for today.');

        foreach ($events as $event) {
            $description = 'Heute um '.Carbon::parse($event->datum)->format('H:i').' Uhr';
            SendPushNotification::dispatch($event->id, $event->name, $description, 'event');
        }

        return 0;
    }
}
