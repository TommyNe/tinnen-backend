<?php

namespace App\Actions\MobileNotifications;

use App\Exceptions\Notifications\TokenNotFound;
use App\Exceptions\Notifications\UnsupportedDeviceType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeleteToken
{
    /**
     * @throws UnsupportedDeviceType
     * @throws TokenNotFound
     */
    public function __invoke(string $deviceType, string $token): void
    {
        if ($deviceType !== 'android' && $deviceType !== 'ios') {
            throw new UnsupportedDeviceType('Unsupported device type');
        }

        $deviceToken = DeviceToken::firstWhere(['token' => $token, 'device_type' => $deviceType]);
        if ($deviceToken === null) {
            throw new TokenNotFound('No token found');
        }

        DB::transaction(function () use ($deviceToken) {
            PendingNotification::where('device_token_id', '=', $deviceToken->id)
                ->where('device_token_id', '<>', null)
                ->update(['device_token_id' => null]);
            $deviceToken->delete();

            Log::notice('Remove device token');
        });
    }
}
