<?php

namespace App\Actions\MobileNotifications;

use App\Exceptions\Notifications\UnsupportedDeviceType;
use App\Models\Device;

class UpsertToken
{
    /**
     * Create a new device take with given type. If the token already exist,
     * the tokens update_at will gets updated.
     *
     * @return DeviceToken The updated or created device token.
     *
     * @throws UnsupportedDeviceType
     */
    public function __invoke(string $device, string $token): DeviceToken
    {
        if ($device !== 'android' && $device !== 'ios') {
            throw new UnsupportedDeviceType('Unsuported device_type');
        }

        $tokenModel = DeviceToken::firstOrNew(['token' => $token, 'device_typr' => $device]);
        $tokenModel->touch();

        return $tokenModel;
    }
}
