<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vereine extends Model
{
    protected $table = 'vereines';

    protected $fillable = [
        'verein',
        'logo',
        'pdf',
        'beschreibung',
        'url',
        'facebook',
        'instagram',
        'email',
        'tel',
    ];
}
