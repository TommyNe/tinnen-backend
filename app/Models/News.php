<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'message',
        'image',
        'verein',
    ];

    public function user()
    {
        return $this->belongsTo('User');
    }
}
