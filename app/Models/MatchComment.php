<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MatchComment extends Model
{
    protected $table = 'match_comments';

    protected $fillable = [
        'comment',
        'match_id',
        'count',
    ];

    public function match(): BelongsTo
    {
        return $this->belongsTo(Matche::class, 'match_id', 'id');
    }
}
