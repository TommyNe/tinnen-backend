<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Termine extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'beschreibung',
        'verein',
        'datum',
        'eventOrt',
    ];
}
