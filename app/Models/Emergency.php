<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Emergency extends Model
{
    protected $table = 'emergency';

    protected $fillable = [
        'title',
        'tel',
        'content',
    ];

    public function emergencyAddress(): HasMany
    {
        return $this->hasMany(EmergencyAddress::class, 'emergency_id', 'id');
    }
}
