<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmergencyAddress extends Model
{
    use HasFactory;

    protected $table = 'emergency_address';

    protected $fillable = [
        'title',
        'tel',
        'url',
        'address',
    ];
}
