<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchGoal extends Model
{
    protected $table = 'match_goals';

    protected $fillable = [
        'match_id',
        'goal_home',
        'goal_away',
        'time',
    ];
}
