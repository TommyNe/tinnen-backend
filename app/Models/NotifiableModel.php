<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotifiableModel extends Model
{
    public function routeNotificationForExpo($notification)
    {
        return 'ExponentPushToken[xxxxxxxxxxxxxxxxxxxxxx]';
    }
}
