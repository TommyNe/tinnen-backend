<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Matche extends Model
{
    protected $table = 'matches';

    protected $fillable = [
        'home_team',
        'away_team',
        'home_team_logo',
        'away_team_logo',
        'is_live',
    ];

    public function goals(): HasMany
    {
        return $this->hasMany(MatchGoal::class, 'match_id', 'id');
    }

    public function match_comments(): HasMany
    {
        return $this->hasMany(MatchComment::class, 'match_id', 'id');
    }
}
