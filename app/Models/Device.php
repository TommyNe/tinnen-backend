<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Device extends Model
{
    use Notifiable;

    public const DEVICE_IOS = 'ios';

    public const DEVICE_ANDROID = 'android';

    protected $fillable = [
        'token',
        'type',
        'is_active',
    ];
}
