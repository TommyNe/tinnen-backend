<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FoerderVerein extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(protected Request $request)
    {
        //
    }

    public function build()
    {
        // The request instance must be passed to the view...
        return $this->markdown('emails.formular', [
            'request' => $this->request,
        ]);
    }
}
