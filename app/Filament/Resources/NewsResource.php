<?php

namespace App\Filament\Resources;

use App\Filament\Resources\NewsResource\Pages;
use App\Models\News;
use AskerAkbar\GptTrixEditor\Components\GptTrixEditor;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class NewsResource extends Resource
{
    protected static ?string $model = News::class;

    protected static ?string $navigationGroup = 'App Content';

    protected static ?string $navigationLabel = 'Aktuelles';

    protected static ?string $modelLabel = 'Aktuelles';

    protected static ?string $pluralModelLabel = 'Aktuelles';

    protected static ?string $pluralLabel = 'Aktuelles';

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->columns(12)
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->columnSpan(12)
                    ->required()
                    ->label('Titel'),
                GptTrixEditor::make('message')
                    ->columnSpan(12)
                    ->required()
                    ->label('Inhalt'),
                //                Forms\Components\RichEditor::make('message')
                //                    ->columnSpan(12)
                //                    ->required()
                //                    ->label('Inhalt'),
                Forms\Components\Select::make('verein')
                    ->columnSpan(6)
                    ->required()
                    ->label('Aktuelles von')
                    ->options([
                        'Allgemein' => 'Allgemein',
                        'DJK Tinnen' => 'DJK Tinnen',
                        'St. Bernadus' => 'St. Bernadus',
                        'Kirchengemeinde' => 'Kirchengemeinde',
                        'Tinner Jäger Musikanten' => 'Tinner Jäger Musikanten',
                        'Jugend Blasorchester' => 'Jugend Blasorchester',
                        'Oldtimer Club' => 'Oldtimer Club',
                        'Motorad Club' => 'Motorad Club',
                        'Imker Verein' => 'Imker Verein',
                        'KLJB Tinnen' => 'KLJB Tinnen',
                        'KFD Tinnen' => 'KFD Tinnen',
                        'Michale Schule' => 'Michael Schule',
                    ]),
                Forms\Components\FileUpload::make('image')
                    ->columnSpan(6)
                    ->directory('news')
                    ->image(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->searchable()
                    ->label('Titel'),
                Tables\Columns\TextColumn::make('message')
                    ->searchable()
                    ->toggleable()
                    ->limit(30)
                    ->label('Inhalt'),
                Tables\Columns\TextColumn::make('verein')
                    ->searchable()
                    ->label('Aktuelles von')
                    ->toggleable(),
                Tables\Columns\ImageColumn::make('image')
                    ->label('Bild')
                    ->toggleable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->label('Erstellt am')
                    ->date('d.m.Y H:i:s')
                    ->sortable()
                    ->toggleable(),
            ])
            ->defaultSort('created_at', 'desc')
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make()
                    ->label('Bearbeiten'),
                Tables\Actions\DeleteAction::make()
                    ->label('Löschen'),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageNews::route('/'),
        ];
    }
}
