<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TermineResource\Pages;
use App\Models\Termine;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class TermineResource extends Resource
{
    protected static ?string $model = Termine::class;

    protected static ?string $navigationGroup = 'App Content';

    protected static ?string $navigationLabel = 'Termine';

    protected static ?string $modelLabel = 'Termin';

    protected static ?string $pluralModelLabel = 'Termine';

    protected static ?string $pluralLabel = 'Termine';

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label('Titel')
                    ->required(),
                Forms\Components\Textarea::make('beschreibung')
                    ->label('Beschreibung')
                    ->required(),
                Forms\Components\Select::make('verein')
                    ->options([
                        'Allgemein' => 'Allgemein',
                        'DJK Tinnen' => 'DJK Tinnen',
                        'St. Bernadus' => 'St. Bernadus',
                        'Kirchengemeinde' => 'Kirchengemeinde',
                        'Tinner Jäger Musikanten' => 'Tinner Jäger Musikanten',
                        'Jugend Blasorchester' => 'Jugend Blasorchester',
                        'Oldtimer Club' => 'Oldtimer Club',
                        'Motorad Club' => 'Motorad Club',
                        'Imker Verein' => 'Imker Verein',
                        'KLJB Tinnen' => 'KLJB Tinnen',
                        'KFD Tinnen' => 'KFD Tinnen',
                        'Michale Schule' => 'Michael Schule',
                    ]),
                Forms\Components\DateTimePicker::make('datum')
                    ->default(now())
                    ->label('Datum')
                    ->required(),
                Forms\Components\TextInput::make('eventOrt')
                    ->label('Ort')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label('Titel')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('beschreibung')
                    ->label('Beschreibung')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('verein')
                    ->label('Verein')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('datum')
                    ->label('Datum')
                    ->dateTime('d.m.Y H:i')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('eventOrt')
                    ->label('Ort')
                    ->searchable()
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make()
                    ->label('Bearbeiten'),
                Tables\Actions\DeleteAction::make()
                    ->label('Löschen'),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageTermines::route('/'),
        ];
    }
}
