<?php

namespace App\Filament\Resources\AppsiteResource\Pages;

use App\Filament\Resources\AppsiteResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAppsite extends EditRecord
{
    protected static string $resource = AppsiteResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
