<?php

namespace App\Filament\Resources\AppsiteResource\Pages;

use App\Filament\Resources\AppsiteResource;
use Filament\Resources\Pages\CreateRecord;

class CreateAppsite extends CreateRecord
{
    protected static string $resource = AppsiteResource::class;
}
