<?php

namespace App\Filament\Resources\NewsResource\Pages;

use App\Filament\Resources\NewsResource;
use App\Jobs\SendPushNotification;
use App\Models\News;
use Bus;
use Filament\Actions\CreateAction;
use Filament\Resources\Pages\ManageRecords;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ManageNews extends ManageRecords
{
    protected static string $resource = NewsResource::class;

    protected function getHeaderActions(): array
    {
        return [
            CreateAction::make()
                ->using(function (array $data): Model {
                    $news = null;
                    DB::transaction(function () use ($data, &$news) {
                        $data['user_id'] = auth()->id();
                        $news = News::create($data);
                    });
                    Bus::chain([
                        new SendPushNotification(
                            $news->id,
                            $data['title'],
                            $data['message'],
                            'news/details'
                        ),
                    ])->dispatch();

                    //                $deepLink = [
                    //                    'deepLink' => [
                    //                        'route' => 'news/details',
                    //                        'params' => [
                    //                            'id' => $news->id
                    //                        ]
                    //                    ]
                    //                ];
                    //                // ToDo: Push Notification over Queue
                    //
                    //                $notification = new ExpoPushNotification();
                    //                foreach (Device::all()->chunk(100) as $group) {
                    //                    $device = $group->where('is_active', '=', true)->pluck('token')->toArray();
                    //                    $notification->sendPushNotification($device, $data['title'], strip_tags($data['message']), $deepLink);
                    //                }
                    return $news;
                })->label('News hinzufügen'),
        ];
    }
}
