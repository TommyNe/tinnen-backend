<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MatcheResource\Pages;
use App\Filament\Resources\MatcheResource\RelationManagers;
use App\Models\Matche;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class MatcheResource extends Resource
{
    protected static ?string $model = Matche::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $modelLabel = 'Liveticker';

    protected static ?string $pluralModelLabel = 'Liveticker';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('home_team')
                    ->label('Heim Mannschaft'),
                Forms\Components\TextInput::make('away_team')
                    ->label('Gast Mannschaft'),
                Forms\Components\FileUpload::make('home_team_logo')
                    ->label('Logo Heim Mannschaf')
                    ->directory('liveticker')
                    ->image(),
                Forms\Components\FileUpload::make('away_team_logo')
                    ->label('Logo Gast Mannschaft')
                    ->directory('liveticker')
                    ->image(),
                Forms\Components\Toggle::make('is_live')
                    ->label('Aktivieren'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('created_at')
                    ->date('d.m.Y H:i')
                    ->toggleable()
                    ->label('Erstellt'),
                Tables\Columns\TextColumn::make('home_team')
                    ->label('Heimmannschaft'),
                Tables\Columns\TextColumn::make('away_team')
                    ->label('Gastmannschaft'),
                Tables\Columns\IconColumn::make('is_live')
                    ->Boolean()
                    ->label('Aktiviert'),
            ])
            ->filters([
                //
            ])
            ->defaultSort('created_at', 'desc')
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\MatchGoalRelationManager::class,
            RelationManagers\MatchCommentsRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMatches::route('/'),
            'create' => Pages\CreateMatche::route('/create'),
            'edit' => Pages\EditMatche::route('/{record}/edit'),
        ];
    }
}
