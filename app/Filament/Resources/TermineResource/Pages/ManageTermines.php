<?php

namespace App\Filament\Resources\TermineResource\Pages;

use App\Filament\Resources\TermineResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageTermines extends ManageRecords
{
    protected static string $resource = TermineResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->label('Termin hinzufügen'),
        ];
    }
}
