<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AppsiteResource\Pages;
use App\Models\Appsite;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class AppsiteResource extends Resource
{
    protected static ?string $model = Appsite::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationLabel = 'App Inhalte';

    protected static ?string $modelLabel = 'App Inhalte';

    public static function form(Form $form): Form
    {
        return $form
            ->columns(12)
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->columnSpan(12)
                    ->autofocus()
                    ->required()
                    ->label('Titel'),
                Forms\Components\RichEditor::make('content')
                    ->columnSpan(12)
                    ->required()
                    ->label('Inhalt'),
                Forms\Components\FileUpload::make('image')
                    ->columnSpan(6)
                    ->directory('appsites')
                    ->image()
                    ->label('Bild'),
                Forms\Components\Select::make('content_id')
                    ->columnSpan(6)
                    ->options([
                        'historie' => 'Historie',
                        'impressum' => 'Impressum',
                        'datenschutz' => 'Datenschutz',
                    ])
                    ->required()
                    ->label('Inhalt ID'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Titel')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('content_id')
                    ->label('Seite')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('content')
                    ->label('Inhalt')
                    ->searchable()
                    ->words(10)
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAppsites::route('/'),
            'create' => Pages\CreateAppsite::route('/create'),
            'edit' => Pages\EditAppsite::route('/{record}/edit'),
        ];
    }
}
