<?php

namespace App\Filament\Resources;

use App\Filament\Resources\EmergencyResource\Pages;
use App\Filament\Resources\EmergencyResource\RelationManagers;
use App\Models\Emergency;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class EmergencyResource extends Resource
{
    protected static ?string $model = Emergency::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationGroup = 'App Content';

    protected static ?string $navigationLabel = 'Notdienste';

    protected static ?string $modelLabel = 'Notdienst';

    protected static ?string $pluralModelLabel = 'Notdienste';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->label('Titel')
                    ->autofocus()
                    ->required(),
                Forms\Components\TextInput::make('tel')
                    ->label('Telefonnummer')
                    ->autofocus()
                    ->required(),
                Forms\Components\TextInput::make('content')
                    ->label('Inhalt')
                    ->autofocus()
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Titel')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('tel')
                    ->label('Telefonnummer')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('content')
                    ->label('Inhalt')
                    ->searchable()
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageEmergencies::route('/'),
        ];
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\EmergencyAddressRelationManager::class,
        ];
    }
}
