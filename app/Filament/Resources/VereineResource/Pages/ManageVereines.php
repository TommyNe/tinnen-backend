<?php

namespace App\Filament\Resources\VereineResource\Pages;

use App\Filament\Resources\VereineResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageVereines extends ManageRecords
{
    protected static string $resource = VereineResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->label('Verein hinzufügen'),
        ];
    }
}
