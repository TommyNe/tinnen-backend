<?php

namespace App\Filament\Resources\MatcheResource\Pages;

use App\Filament\Resources\MatcheResource;
use App\Models\Matche;
use App\Models\MatchGoal;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CreateMatche extends CreateRecord
{
    protected static string $resource = MatcheResource::class;

    protected function handleRecordCreation(array $data): Model
    {
        $home = '0';
        $away = '0';
        $minute = '0';
        $matche = null;
        DB::transaction(function () use ($data, &$matche, $home, $away, $minute) {
            $matche = Matche::create($data);
            $goals = MatchGoal::create([
                'match_id' => $matche->id,
                'goal_home' => $home,
                'goal_away' => $away,
                'time' => $minute,
            ]);
        });

        return $matche;
    }
}
