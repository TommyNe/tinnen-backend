<?php

namespace App\Filament\Resources\MatcheResource\RelationManagers;

use App\Http\Services\ExpoPushNotification;
use App\Models\Device;
use App\Models\MatchGoal;
use App\Models\PushNotification;
use Bus;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MatchGoalRelationManager extends RelationManager
{
    protected static string $relationship = 'goals';

    protected static ?string $modelLabel = 'Ergebnisse';

    protected static ?string $pluralModelLabel = 'Ergebnisse';

    protected static ?string $recordTitleAttribute = 'match_id';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('goal_home')
                    ->label('Tor Heimmanschaft'),
                Forms\Components\TextInput::make('goal_away')
                    ->label('Tor Gastmannschaft'),
                Forms\Components\TextInput::make('time')
                    ->label('Minute'),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id'),
                Tables\Columns\TextColumn::make('match_id'),
                Tables\Columns\TextColumn::make('goal_home')
                    ->label('Tore Heimmannschaft'),
                Tables\Columns\TextColumn::make('goal_away')
                    ->label('Tore Gastmannschaft'),
                TextColumn::make('time')
                    ->label('Minute'),
            ])
            ->defaultSort('id', 'asc')
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make()
                    ->using(function (array $data, RelationManager $livewire): Model {
                        $goal = null;
                        //                        $deepLink = [
                        //                            'deepLink' => [
                        //                                'route' => 'liveticker',
                        //                                'params' => [
                        //                                    'id' => $livewire->ownerRecord->id
                        //                                ]
                        //                            ]
                        //                        ];
                        DB::transaction(function () use ($data, &$goal, $livewire) {
                            $data['user_id'] = auth()->id();
                            $data['match_id'] = $livewire->ownerRecord->id;
                            $goal = MatchGoal::create($data);
                        });

                        $message = $data['goal_home'].':'.$data['goal_away'];
                        $title = 'Tor in der '.$data['time'].' Minute';
                        $match = $livewire->ownerRecord;

                        if ($data['time'] === '1') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Anpfiff';
                        }
                        if ($data['time'] === '45') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Halbzeit';
                        }

                        if ($data['time'] === '46') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Anpfiff 2. Halbzeit';
                        }

                        if ($data['time'] === '90') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Abpfiff';
                        }

                        //                        $notification = new ExpoPushNotification();
                        //                        foreach (Device::all()->chunk(100) as $group) {
                        //                            $device = $group->where('is_active', '=', true)->pluck('token')->toArray();
                        //                            $notification->sendPushNotification($device, $title, strip_tags($message), $deepLink);
                        //                        }

                        Bus::chain([
                            new \App\Jobs\SendPushNotification($livewire->ownerRecord->id, $title, $message, 'liveticker'),
                        ])->dispatch();

                        // ToDo: Push Notification over Queue

                        //                            $push = PushNotification::create([
                        //                                'title' => $title,
                        //                                'body' => $message,
                        //                                'route' => 'Liveticker',
                        //                                'params' => $livewire->ownerRecord->id,
                        //                                'is_sent' => false
                        //                            ]);
                        //
                        //                            \App\Jobs\PushNotification::dispatch();

                        return $goal;
                    }),
            ])
            ->actions([
                Tables\Actions\EditAction::make()
                    ->using(function (Model $record, array $data, RelationManager $livewire): Model {
                        $data['user_id'] = auth()->id();
                        $data['match_id'] = $livewire->ownerRecord->id;
                        $record->update($data);
                        //                        $deepLink = [
                        //                            'deepLink' => [
                        //                                'route' => 'liveticker',
                        //                                'params' => [
                        //                                    'id' => $livewire->ownerRecord->id
                        //                                ]
                        //                            ]
                        //                        ];

                        $message = $data['goal_home'].':'.$data['goal_away'];
                        $title = 'Tor in der '.$data['time'].' Minute';
                        $match = $livewire->ownerRecord;

                        if ($data['time'] === '1') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Anpfiff';
                        }
                        if ($data['time'] === '45') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Halbzeit';
                        }

                        if ($data['time'] === '46') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Anpfiff 2. Halbzeit';
                        }

                        if ($data['time'] === '90') {
                            $title = $match->home_team.' - '.$match->away_team.' '.'Abpfiff';
                        }

                        Bus::chain([
                            new \App\Jobs\SendPushNotification($livewire->ownerRecord->id, $title, $message, 'liveticker'),
                        ])->dispatch();

                        //                        $notification = new ExpoPushNotification();
                        //                        foreach (Device::all()->chunk(100) as $group) {
                        //                            $device = $group->where('is_active', '=', true)->pluck('token')->toArray();
                        //                            $notification->sendPushNotification($device, $title, strip_tags($message), $deepLink);
                        //                        }

                        // ToDo: Push Notification over Queue

                        //                            $push = PushNotification::create([
                        //                                'title' => $title,
                        //                                'body' => $message,
                        //                                'route' => 'Liveticker',
                        //                                'params' => $livewire->ownerRecord->id,
                        //                                'is_sent' => false
                        //                            ]);
                        //
                        //                            \App\Jobs\PushNotification::dispatch();

                        return $record;
                    }),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
}
