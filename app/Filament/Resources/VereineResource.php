<?php

namespace App\Filament\Resources;

use App\Filament\Resources\VereineResource\Pages;
use App\Models\Vereine;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class VereineResource extends Resource
{
    protected static ?string $model = Vereine::class;

    protected static ?string $navigationGroup = 'App Content';

    protected static ?string $navigationLabel = 'Vereine';

    protected static ?string $modelLabel = 'Verein';

    protected static ?string $pluralModelLabel = 'Vereine';

    protected static ?string $pluralLabel = 'Vereine';

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->columns(12)
            ->schema([
                Forms\Components\TextInput::make('verein')
                    ->columnSpan(12)
                    ->label('Verein'),
                Forms\Components\RichEditor::make('beschreibung')
                    ->columnSpan(12)
                    ->label('Beschreibung'),
                Forms\Components\TextInput::make('url')
                    ->columnSpan(6)
                    ->label('URL'),
                Forms\Components\TextInput::make('facebook')
                    ->columnSpan(6)
                    ->label('Facebook'),
                Forms\Components\TextInput::make('instagram')
                    ->columnSpan(6)
                    ->label('Instagram'),
                //                Forms\Components\TextInput::make('twitter')
                //                ->label('Twitter'),
                //                Forms\Components\TextInput::make('youtube')
                //                ->label('Youtube'),
                Forms\Components\TextInput::make('email')
                    ->columnSpan(6)
                    ->label('E-Mail'),
                Forms\Components\TextInput::make('tel')
                    ->columnSpan(6)
                    ->label('Telefon'),
                Forms\Components\FileUpload::make('logo')
                    ->columnSpan(6)
                    ->directory('vereine')
                    ->label('Logo')
                    ->image(),
                Forms\Components\FileUpload::make('pdf')
                    ->columnSpan(6)
                    ->directory('vereine')
                    ->label('Flyer')
                    ->acceptedFileTypes(['application/pdf']),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('verein')
                    ->label('Verein'),
                Tables\Columns\TextColumn::make('beschreibung')
                    ->label('Beschreibung'),
                Tables\Columns\TextColumn::make('url')
                    ->label('URL'),
                Tables\Columns\TextColumn::make('facebook')
                    ->label('Facebook'),
                Tables\Columns\TextColumn::make('instagram')
                    ->label('Instagram'),
                //                Tables\Columns\TextColumn::make('twitter')
                //                ->label('Twitter'),
                //                Tables\Columns\TextColumn::make('youtube')
                //                ->label('Youtube'),
                Tables\Columns\TextColumn::make('email')
                    ->label('E-Mail'),
                Tables\Columns\TextColumn::make('tele')
                    ->label('Telefon'),
                Tables\Columns\ImageColumn::make('logo')
                    ->url('logo')
                    ->label('Logo'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make()
                    ->label('Bearbeiten'),
                Tables\Actions\DeleteAction::make()
                    ->label('Löschen'),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageVereines::route('/'),
        ];
    }
}
