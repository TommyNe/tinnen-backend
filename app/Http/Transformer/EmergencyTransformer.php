<?php

namespace App\Http\Transformer;

use App\Models\Emergency;

class EmergencyTransformer
{
    public function format(Emergency $emergency): array
    {
        $transformed = [
            'id' => $emergency->id,
            'title' => $emergency->title,
            'tel' => $emergency->tel,
            'content' => [],
            'created_at' => $emergency->created_at->toDateTimeString(),
            'updated_at' => $emergency->updated_at->toDateTimeString(),
        ];

        foreach ($emergency->emergencyAddress as $emergencyAddress) {
            $transformed['content'][] = [
                'id' => $emergencyAddress->id,
                'title' => $emergencyAddress->title,
                'tel' => $emergencyAddress->tel,
                'url' => $emergencyAddress->url,
                'address' => $emergencyAddress->address,
                'created_at' => $emergencyAddress->created_at->toDateTimeString(),
                'updated_at' => $emergencyAddress->updated_at->toDateTimeString(),
            ];
        }

        return $transformed;
    }
}
