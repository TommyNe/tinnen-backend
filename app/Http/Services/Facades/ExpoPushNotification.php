<?php

namespace App\Http\Services\Facades;

use Illuminate\Support\Facades\Facade;

class ExpoPushNotification extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \App\Http\Services\ExpoPushNotification::class;
    }
}
