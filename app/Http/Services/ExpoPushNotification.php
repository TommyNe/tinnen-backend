<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ExpoPushNotification
{
    public function sendPushNotification($recipients, $title, $body, $data): void
    {
        $response = Http::post('https://exp.host/--/api/v2/push/send', [
            'to' => $recipients,
            'title' => $title,
            'sound' => 'default',
            'body' => $body,
            'data' => $data,
        ])->json();
        Log::info($response);
    }
}
