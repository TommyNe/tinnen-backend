<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\User;
use App\Notifications\OffersNotification;
use Notification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendPushNotification($message)
    {
        $userSchema = User::first();
        $devices = Device::all()->pluck('token')->toArray();

        $pushData = [
            'title' => $message['title'],
            'body' => $message['content'],
        ];

        Notification::send($devices, new OffersNotification($pushData));
    }
}
