<?php

namespace App\Http\Controllers;

use App\Http\Services\ExpoPushNotification;
use App\Models\Device;
use App\Models\Termine;
use Illuminate\Http\Request;

class TermineController extends Controller
{
    public function __construct(private ExpoPushNotification $notification) {}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $termine = Termine::orderBy('datum', 'asc')->paginate(10);

        return view('termine.index', [
            'termine' => $termine,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datetime = new \DateTime('now');

        return view('termine.create', [
            'datetime' => $datetime,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $devices = Device::all()->pluck('token')->toArray();

        $termine = new Termine;
        $termine->name = $request->input('title');
        $termine->beschreibung = $request->input('description');
        $termine->verein = $request->input('verein');
        $termine->datum = $request->input('date');
        $termine->eventOrt = $request->input('eventOrt');
        $termine->save();

        $data = [
            'deepLink' => [
                'route' => 'EventDetails',
                'params' => [
                    'id' => $termine->id,
                ],
            ],
        ];

        $this->notification->sendPushNotification($devices, $request->input('title'), $request->input('date'), $data);

        return redirect()->route('termine.index')->with('success', 'Termin has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Termine $termine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Termine $termine)
    {
        return view('termine.edit', [
            'termine' => $termine,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Termine $termine)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'verein' => 'required',
            'date' => 'required',
        ]);
        $termine->name = $request->title;
        $termine->beschreibung = $request->description;
        $termine->verein = $request->verein;
        $termine->datum = $request->date;
        $termine->eventOrt = $request->eventOrt;
        $termine->save();

        return redirect()->route('termine.index')->with('success', 'Termin Has Been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Termine $termine)
    {
        $termine->delete();

        return redirect()->route('termine.index')->with('success', 'Termin has been deleted successfully');

    }
}
