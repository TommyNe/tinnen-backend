<?php

namespace App\Http\Controllers;

use App\Models\Vereine;
use Illuminate\Http\Request;

class VereineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $vereine = Vereine::all()->sortBy('verein');

        return view('vereine.index', [
            'vereine' => $vereine,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vereine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = $request->file('file')->getClientOriginalName();
        $request->file->move(public_path('logos'), $fileName);

        $vereine = new Vereine;
        $vereine->logo = $fileName;
        $vereine->verein = $request->input('vereine');
        $vereine->beschreibung = $request->input('description');
        $vereine->url = $request->input('url');
        $vereine->facebook = $request->input('facebook');
        $vereine->instagram = $request->input('instagram');
        $vereine->email = $request->input('email');
        $vereine->tel = $request->input('tel');
        $vereine->save();

        return redirect()->route('vereine.index')->with('success', 'Verein has been create successfully');
    }

    /**
     * Display the specified resource.
     *

     * @return \Illuminate\Http\Response
     */
    public function show(Vereine $vereine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *

     * @return \Illuminate\Http\Response
     */
    public function edit(Vereine $vereine)
    {
        return view('vereine.edit', [
            'vereine' => $vereine,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vereine $vereine)
    {

        $request->validate([
            'vereine' => 'required',
            'description' => 'required',
        ]);

        $vereine->verein = $request->vereine;

        if ($request->file('file')) {
            $fileName = $request->file('file')->getClientOriginalName();
            $request->file->move(public_path('logos'), $fileName);
            $vereine->logo = $fileName;
        }
        $vereine->beschreibung = $request->description;
        $vereine->url = $request->url;
        $vereine->facebook = $request->facebook;
        $vereine->instagram = $request->instagram;
        $vereine->email = $request->email;
        $vereine->tel = $request->tel;
        $vereine->save();

        return redirect()->route('vereine.index')->with('success', 'Verein has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *

     * @return \Illuminate\Http\Response
     */
    public function destroy(Vereine $vereine)
    {
        $vereine->delete();

        return redirect()->route('vereine.index')->with('success', 'Verein has been deleted successfully');
    }
}
