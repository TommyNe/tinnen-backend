<?php

namespace App\Http\Controllers;

use App\Http\Services\ExpoPushNotification;
use App\Models\Device;
use App\Models\News;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function __construct(private ExpoPushNotification $notification) {}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(10);

        return view('news.index', [
            'news' => $news,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $devices = Device::all()->pluck('token')->toArray();

        //        $news = new News();
        //        $news->user_id = Auth::id();
        //        $news->title = $request->input('title');
        //        $news->message = $request->input('description');
        //        $fileName = $request->file('file');
        //        $pic = md5(uniqid()) . '.' . $fileName->guessClientExtension();
        //        $request->file->move(public_path('images'), $pic);
        //        $news->image = $pic;
        //        $news->verein = $request->input('verein');
        //        $news->save();

        $data = [
            'deepLink' => [
                'route' => 'NewsDetails',
                'params' => [
                    'id' => '1',
                ],
            ],
        ];
        foreach ($devices as $device) {
            $this->notification->sendPushNotification($device, $request->input('title'), $request->input('description'), $data);

        }

        return redirect()->route('news.index')->with('success', 'News has been create successfully');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('news.edit', [
            'news' => $news,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'verein' => 'required',
        ]);
        if ($request->file('file')) {
            $fileName = $request->file('file')->getClientOriginalName();
            $request->file->move(public_path('images'), $fileName);
            $news->image = $fileName;
        }

        $news->title = $request->title;
        $news->message = $request->description;
        $news->verein = $request->verein;
        $news->user_id = Auth::id();
        $news->save();

        return redirect()->route('news.index')->with('success', 'News has been update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();

        return redirect()->route('news.index')->with('success', 'News has been deleted successfully');
    }
}
