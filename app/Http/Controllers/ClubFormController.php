<?php

namespace App\Http\Controllers;

use App\Mail\FoerderVerein;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ClubFormController extends Controller
{
    public function index()
    {
        return view('club.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'last-name' => 'required|string',
            'first-name' => 'required|string',
            'street' => 'required|string',
            'location' => 'required|string',
            'plz' => 'required|string',
            'email' => 'required|email',
            'iban' => 'required|regex:/^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$/',
            'price' => 'required|numeric',
            'sepa' => 'required|accepted',
            'statute' => 'required|accepted',
        ]);
        Mail::to('foerderverein.tinnen@gmail.com')->send(new FoerderVerein($request));

        // foerderverein.tinnen@gmail.com
        return redirect()->route('foerderverein.index')->with('success', 'Beitrittserklärung wurde gesendet!');
    }
}
