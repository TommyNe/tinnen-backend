<?php

namespace App\Http\Controllers\API;

use App\Query\MatchesQueryFactory;

class MatchesController
{
    public function index()
    {
        $matches = MatchesQueryFactory::getMatchesQuery()->get();

        $live = [];
        foreach ($matches as $match) {
            $goal = $match->goals->last();
            $data = [
                'id' => $match->id,
                'timecount' => $goal->time,
                'team1' => $match->home_team,
                'team2' => $match->away_team,
                'isLive' => $match->is_live,
                'team1logo' => $match->home_team_logo,
                'team2logo' => $match->away_team_logo,
                'team1goal' => $goal->goal_home,
                'team2goal' => $goal->goal_away,
                'title_comment' => $match->match_comments->last(),
                'comments' => $match->match_comments,
            ];
            $live[] = $data;

        }

        return response()->json($live);
    }
}
