<?php

namespace App\Http\Controllers\API;

use App\Models\Appsite;

class DatenschutzController
{
    public function index()
    {
        $datenschutz = Appsite::where('content_id', 'datenschutz')->get();
        $datenschutz->toArray();

        return response()->json([
            'data' => $datenschutz,
        ]);
    }
}
