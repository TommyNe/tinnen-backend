<?php

namespace App\Http\Controllers\API;

use App\Models\News;
use App\Query\NewsQueryFactory;
use Illuminate\Http\JsonResponse;

class NewsController extends ApiController
{
    public function index(): JsonResponse
    {
        $news = NewsQueryFactory::getNewsQuery()->paginate();

        return response()->json([
            'data' => $news,
        ]);
    }

    public function show(News $news): JsonResponse
    {
        return response()->json([
            'data' => $news,
        ]);
    }
}
