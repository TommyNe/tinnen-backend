<?php

namespace App\Http\Controllers\API;

use App\Query\ImageQueryFactory;
use Illuminate\Http\JsonResponse;

class ImageController extends ApiController
{
    public function index(): JsonResponse
    {
        $image = ImageQueryFactory::getImageQuery()->get();

        return response()->json([
            'data' => $image,
        ]);
    }
}
