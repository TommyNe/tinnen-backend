<?php

namespace App\Http\Controllers\API;

use App\Query\SponsorQueryFactory;

class SponsorController
{
    public function index()
    {
        $sponsor = SponsorQueryFactory::getSponsorQuery()->get();

        return response()->json([
            'data' => $sponsor,
        ]);
    }
}
