<?php

namespace App\Http\Controllers\API;

use App\Models\Appsite;

class HistorieController extends ApiController
{
    public function index()
    {
        $historie = Appsite::where('content_id', 'historie')->get();
        $historie->toArray();

        return response()->json([
            'data' => $historie,
        ]);
    }
}
