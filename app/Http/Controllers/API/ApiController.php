<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Device;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    public function token(Request $request): JsonResponse
    {
        $validated = $request->validate([
            'token' => 'required',
            'type' => 'in:ios,android',
            'is_active' => 'boolean',
        ]);

        Device::updateOrCreate(
            [
                'token' => $validated['token'],
                'type' => $validated['type'],
            ],
            [
                'is_active' => $validated['is_active'],
            ]
        );

        return Response::json(['message' => 'ok']);
    }
}
