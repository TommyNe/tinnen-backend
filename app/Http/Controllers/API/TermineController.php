<?php

namespace App\Http\Controllers\API;

use App\Models\Termine;
use App\Query\TerminQueryFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TermineController extends ApiController
{
    public function index(Request $request): JsonResponse
    {
        $param = $request->verein ?? null;

        if ($param) {
            $termine = TerminQueryFactory::getTerminQuery()
                ->where('verein', $param)
                ->orderBy('datum', 'asc')
                ->paginate();
        } else {
            $termine = TerminQueryFactory::getTerminQuery()->paginate();
        }

        return response()->json([
            'data' => $termine,
        ]);
    }

    public function show(Termine $termine): JsonResponse
    {
        return response()->json([
            'data' => $termine,
        ]);
    }
}
