<?php

namespace App\Http\Controllers\API;

use App\Query\EmergencyQueryFactory;

class EmergencyController
{
    public function index()
    {
        $emergency = EmergencyQueryFactory::getEmergencyQuery()->get();

        return response()->json([
            'data' => $emergency,
        ]);
    }
}
