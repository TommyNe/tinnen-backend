<?php

namespace App\Http\Controllers\API;

use App\Models\Vereine;
use Illuminate\Http\JsonResponse;

class VereineController extends ApiController
{
    public function index(): JsonResponse
    {
        $vereine = Vereine::query()->get();

        return response()->json([
            'data' => $vereine,
        ]);
    }

    public function show(Vereine $vereine): JsonResponse
    {
        return response()->json([
            'data' => $vereine,
        ]);
    }
}
