<?php

namespace App\Http\Controllers\API;

use App\Models\Appsite;

class ImpressumController
{
    public function index()
    {
        $impressum = Appsite::where('content_id', 'impressum')->get();
        $impressum->toArray();

        return response()->json([
            'data' => $impressum,
        ]);
    }
}
