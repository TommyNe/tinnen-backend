<?php

namespace App\Http\Controllers;

use App\Models\Datenschutz;
use Illuminate\Http\Request;

class DatenschutzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('datenschutz.datenschutz');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('datenschutz.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datenschutz = new Datenschutz;
        $datenschutz->title = $request->input('title');
        $datenschutz->content = $request->input('content');

        return redirect()->route('datenschutz.index')->with('success', 'Datenschutz has been create successfully');

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Datenschutz $datenschutz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Datenschutz $datenschutz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datenschutz $datenschutz)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Datenschutz $datenschutz)
    {
        //
    }
}
